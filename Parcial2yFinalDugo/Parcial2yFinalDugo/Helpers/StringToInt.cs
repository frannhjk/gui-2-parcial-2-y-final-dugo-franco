﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parcial2yFinalDugo.Helpers
{
    public static class StringToInt
    {
        public static int parseNumbersFromString(string data)
        {
            string number = "";

            for (int i = 0; i < data.Length; ++i)
            {
                if (char.IsDigit(data[i]))
                    number += data[i];
            }
            return Convert.ToInt32(number);
        }
    }
}