﻿using Parcial2yFinalDugo.ViewModels;
using Service.DTOs;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Parcial2yFinalDugo.Controllers
{
    public class EmpresaController : Controller
    {
        private IEmpresaService service;

        public EmpresaController(IEmpresaService _service)
        {
            service = _service;
        }

        public ActionResult Index(int pagina = 1)
        {
            var cantidadRegistrosPorPagina = 10; // parámetro

            var empresas = this.service.getEmpresasFiltered(pagina, cantidadRegistrosPorPagina);
            var totalDeRegistros = this.service.getEmpresas().Count(); // todo sacar con lo de arriba

            var modelo = new IndexViewModel();

            modelo.Empresas = empresas.ToList();
            modelo.PaginaActual = pagina;
            modelo.TotalDeRegistros = totalDeRegistros;
            modelo.RegistrosPorPagina = cantidadRegistrosPorPagina;

            return View(empresas);
        }


        public ActionResult Index2(int pagina = 1)
        {
            var cantidadRegistrosPorPagina = 10; // parámetro

            var empresas = this.service.getEmpresasFiltered(pagina, cantidadRegistrosPorPagina);
            var totalDeRegistros = this.service.getEmpresas().Count(); // todo sacar con lo de arriba

            var modelo = new IndexViewModel();

            modelo.Empresas = empresas.ToList();
            modelo.PaginaActual = pagina;
            modelo.TotalDeRegistros = totalDeRegistros;
            modelo.RegistrosPorPagina = cantidadRegistrosPorPagina;

            ViewBag.MyListEmpresa = modelo.Empresas;

            return View(modelo);
        }



        public ActionResult EmpresaDetail(int id)
        {
            var empresa = this.service.getEmpresa(id);

            return View(empresa);
        }

        public ActionResult AltaEmpresa()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AltaEmpresa(FormCollection formCollection)
        {
            try
            {
                EmpresaDTO empresaDTO = new EmpresaDTO();

                empresaDTO.NombreComercial = formCollection["NombreComercial"];
                empresaDTO.NumeroFiscal = formCollection["NumeroFiscal"];
                empresaDTO.Area = formCollection["Area"];
                empresaDTO.Direccion = formCollection["Direccion"];
                empresaDTO.Telefono = formCollection["Telefono"];
                empresaDTO.FechaAlta = DateTime.Now;

                if (formCollection["FechaAlta"] == null)
                    empresaDTO.FechaAlta = DateTime.Parse(formCollection["FechaAlta"]);

                if (formCollection["ImagenSrc"] == null || formCollection["ImagenSrc"] == "")
                    empresaDTO.ImagenSrc = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1";
                else
                    empresaDTO.ImagenSrc = formCollection["ImagenSrc"];

                this.service.guardarEmpresa(empresaDTO);

                return RedirectToAction("Index2", "Empresa");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult BajaEmpresa(int id)
        {
            var empresa = this.service.getEmpresa(id);

            return View(empresa);
        }

        [HttpPost]
        public ActionResult BajaEmpresa(int id, string param)
        {
            try
            {
                this.service.bajaEmpresa(id);

                return RedirectToAction("Index2", "Empresa");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult EditarEmpresa(int id)
        {
            var empresa = this.service.getEmpresa(id);

            return View(empresa);
        }

        [HttpPost]
        public ActionResult EditarEmpresa(int id, FormCollection formCollection)
        {
            try
            {
                EmpresaDTO empresaDTO = new EmpresaDTO();

                empresaDTO.NombreComercial = formCollection["NombreComercial"];
                empresaDTO.NumeroFiscal = formCollection["NumeroFiscal"];
                empresaDTO.Area = formCollection["Area"];
                empresaDTO.Direccion = formCollection["Direccion"];
                empresaDTO.Telefono = formCollection["Telefono"];
                empresaDTO.ImagenSrc = formCollection["ImagenSrc"];
                empresaDTO.FechaAlta = DateTime.Now;

                if (formCollection["FechaAlta"] == null)
                    empresaDTO.FechaAlta = DateTime.Parse(formCollection["FechaAlta"]);

                this.service.editarEmpresa(id, empresaDTO);

                return RedirectToAction("Index2", "Empresa");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}