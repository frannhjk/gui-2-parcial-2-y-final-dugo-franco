﻿using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Parcial2yFinalDugo.Controllers
{
    public class HistorialController : Controller
    {
        private IHistorialService service;

        public HistorialController(IHistorialService _service) 
        {
            service = _service;
        }

        // GET: Historial
        public ActionResult Index()
        {
            var dtosResult = this.service.getHistoriales();
            
            return View(dtosResult);
        }
    }
}