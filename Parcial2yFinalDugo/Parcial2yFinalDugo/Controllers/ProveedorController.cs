﻿using Parcial2yFinalDugo.Helpers;
using Service.DTOs;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Parcial2yFinalDugo.Controllers
{
    public class ProveedorController : Controller
    {
        public IProveedorService service;

        public ProveedorController(IProveedorService _service)
        {
            service = _service;
        }

        public ActionResult Index()
        {
            var dtoResult = this.service.getProveedores();

            return View(dtoResult);
        }

        #region Asociar - Desasociar

        public ActionResult VistaAsociarEmpresa(int id)
        {
            try
            {
                var entitiesId = this.service.getEmpresasIds(id);

                var entitiesIdsAndNames = this.service.getEmpresasIdsAndNames(entitiesId);

                ViewBag.MyList = entitiesIdsAndNames;

                return View();
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult VistaAsociarEmpresa(FormCollection formCollection, int id)
         {
            try
            {
                var entityParsedId = StringToInt.parseNumbersFromString(formCollection["EmpresaId"]);

                var empresaId = entityParsedId;

                this.service.Asociar(id, empresaId);
                
                return RedirectToAction("Index", "Proveedor");
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public ActionResult VistaDesAsociarEmpresa(int id)
        {
            try
            {
                var entitiesId = this.service.getHistorialEmpresasIdsByProveedorId(id);

                var entitiesIdsAndNames = this.service.getEmpresasIdsAndNames(entitiesId);

                ViewBag.MyList = entitiesIdsAndNames;

                return View();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult VistaDesAsociarEmpresa(FormCollection formCollection, int id)
        {
            try
            {
                var entityParsedId = StringToInt.parseNumbersFromString(formCollection["EmpresaId"]);

                var empresaId = entityParsedId;

                this.service.DesAsociar(id, empresaId);

                return RedirectToAction("Index", "Proveedor");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion Asociar - Desasociar

        #region Solo Admin 

        public ActionResult BajaProveedor(int id)
        {
            var proveedor = this.service.getProveedor(id);

            return View(proveedor);
        }

        [HttpPost]
        public ActionResult BajaProveedor(int id, string param)
        {
            try
            {
                this.service.bajaProveedor(id);

                return RedirectToAction("Index", "Proveedor");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult EditarProveedor(int id)
        {
            var proveedor = this.service.getProveedorEdit(id);

            return View(proveedor);
        }

        [HttpPost]
        public ActionResult EditarProveedor(int id, FormCollection formCollection)
        {
            try
            {
                var proveedor = new ProveedorEditDTO();

                proveedor.Nombre = formCollection["Nombre"];
                proveedor.Localidad = formCollection["Localidad"];

                if (formCollection["ImagenSrc"] is null || formCollection["ImagenSrc"] == "")
                    proveedor.ImagenSrc = "http://4.bp.blogspot.com/-zsbDeAUd8aY/US7F0ta5d9I/AAAAAAAAEKY/UL2AAhHj6J8/s1600/facebook-default-no-profile-pic.jpg";
                else
                    proveedor.ImagenSrc = formCollection["ImagenSrc"];

                this.service.editarProveedor(id, proveedor);

                return RedirectToAction("Index", "Proveedor");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

    }
}