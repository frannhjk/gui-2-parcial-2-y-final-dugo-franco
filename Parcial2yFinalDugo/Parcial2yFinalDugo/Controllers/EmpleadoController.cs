﻿using Microsoft.Ajax.Utilities;
using Parcial2yFinalDugo.Helpers;
using Service.DTOs;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Parcial2yFinalDugo.Controllers
{
    public class EmpleadoController : Controller
    {
        private IEmpleadoService service;

        public EmpleadoController(IEmpleadoService _service)
        {
            service = _service;
        }

        // GET: Empleado
        public ActionResult Index(string searched)
        {
            IEnumerable<EmpleadoDTO> empleados;

            if(searched == null || searched == "")
            {
                empleados = this.service.getEmpleados();
            }
            else
            {
                empleados = this.service.getEmpleadosBySearch(searched); 
            }

            return View(empleados);
        }

        public ActionResult EmpleadoDetails(int id)
        {
            var empleado = this.service.getEmpleado(id);

            return View(empleado);
        }

        public ActionResult AltaEmpleado()
        {
            var entitiesId = this.service.getEmpresasIds();

            var entitiesIdsAndNames = this.service.getEmpresasIdsAndNames(entitiesId);

            ViewBag.MyList = entitiesIdsAndNames;

            return View();
        }

        [HttpPost]
        public ActionResult AltaEmpleado(FormCollection formCollection)
        {
            try
            {
                var empleadoDTO = new EmpleadoDTO();

                empleadoDTO.Nombre = formCollection["Nombre"];
                empleadoDTO.Apellido = formCollection["Apellido"];
                empleadoDTO.SubArea = formCollection["SubArea"];

                if (formCollection["EmpresaId"] == null || formCollection["EmpresaId"] == "")
                    throw new Exception("Debe ingresar un EmpresaId válido");

                var entityParsedId = StringToInt.parseNumbersFromString(formCollection["EmpresaId"]);

                empleadoDTO.EmpresaId = entityParsedId;

                if (!string.IsNullOrEmpty(formCollection["checkResp"]))
                {
                    string checkResp = formCollection["checkResp"];
                    bool checkRespB = Convert.ToBoolean(checkResp);

                    if (checkRespB == true)
                        empleadoDTO.esGerente = true;
                    else
                        empleadoDTO.esGerente = false;
                }

                this.service.guardarEmpleado(empleadoDTO);

                return RedirectToAction("Index", "Empleado");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult BajaEmpleado(int id)
        {
            var empleado = service.getEmpleado(id);

            return View(empleado);
        }

        [HttpPost]
        public ActionResult BajaEmpleado(int id, string param)
        {
            try
            {
                service.bajaEmpleado(id);
                return RedirectToAction("Index", "Empleado");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult EditarEmpleado(int id)
        {
            var empleado = service.getEmpleado(id);

            return View(empleado);
        }

        [HttpPost]
        public ActionResult EditarEmpleado(int id, FormCollection formCollection)
        {
            try
            {
                var empleadoDTO = new EmpleadoDTO();

                empleadoDTO.Nombre = formCollection["Nombre"];
                empleadoDTO.Apellido = formCollection["Apellido"];
                empleadoDTO.SubArea = formCollection["SubArea"];
                empleadoDTO.EmpresaId = Convert.ToInt32(formCollection["EmpresaId"]);
                empleadoDTO.esGerente = false;

                if (formCollection["esGerente"] != null)
                    empleadoDTO.esGerente = true;

                var dto = empleadoDTO;

                this.service.editarEmpleado(id, empleadoDTO);

                return RedirectToAction("Index", "Empleado");
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}