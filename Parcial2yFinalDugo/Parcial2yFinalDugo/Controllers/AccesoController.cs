﻿using Service.DTOs;
using Service.Interfaces;
using Service.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Parcial2yFinalDugo.Controllers
{
    public class AccesoController : Controller
    {
        private IUsuarioService _usuarioService;

        public AccesoController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        // GET: Acceso
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Login(string User, string Password)
        {
            try
            {
                var data = _usuarioService.GetUsuario(User, Crypto.Hash(Password));

                if(data.Id == 0)
                {
                    ViewBag.Error = "Credenciales Incorrectas";
                    return View();
                }

                Session["Nombre"] = data.Email;
                Session["UsuarioRol"] = data.RolId;
                Session["User"] = data;
                Session["Id"] = data.Id;

                return RedirectToAction("Index", "Home");
            }
            catch(Exception e)
            {
                ViewBag.Error = e.Message;
                return View();
            }
        }

        public ActionResult Registro()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registro(FormCollection formCollection)
        {
            try
            {
                if (formCollection["Password"] == "")
                    throw new Exception("El campo password no puede ser null");
                if (formCollection["Email"] == "")
                    throw new Exception("El campo email no puede ser null");
                if (formCollection["Name"] == "")
                    throw new Exception("El campo nombre no puede ser null");
                if (formCollection["Lastname"] == "")
                    throw new Exception("El campo apellido no puede ser null");

                var usuarioDTO = new UsuariosDTO()
                {
                    Name = formCollection["Name"],
                    Lastname = formCollection["Lastname"],
                    Email = formCollection["Email"],
                    Password = Crypto.Hash(formCollection["Password"])
                };

                this._usuarioService.guardarUsuario(usuarioDTO);

                return RedirectToAction("Login", "Acceso");
            }
            catch(Exception e)
            {
                throw e;
            }
        }


        public ActionResult ABMUsuario()
        {
            var usuarios = this._usuarioService.getUsuarios();

            return View(usuarios);
        }

        public ActionResult ABMUsuarioDetails(int id)
        {
            var usuario = this._usuarioService.GetUsuarioById(id);

            return View(usuario);
        }

        public ActionResult ABMUsuariosEdit(int id)
        {
            var usuario = this._usuarioService.GetUsuarioById(id);

            var roles = this._usuarioService.getRoles();

            ViewBag.MyList = roles;

            return View(usuario);
        }

        [HttpPost]
        public ActionResult ABMUsuariosEdit(int id, FormCollection formCollection)
        {
            try
            {
                var usuarioDTO = new UsuariosDTO()
                {
                    Name = formCollection["Name"],
                    Lastname = formCollection["Lastname"],
                    Email = formCollection["Email"],
                    Password = Crypto.Hash(formCollection["Password"]),
                    RolId = Convert.ToInt32(formCollection["RolId"])
                };

                this._usuarioService.ActualizarUsuario(id, usuarioDTO);

                return RedirectToAction("ABMUsuario", "Acceso");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult ABMUsuariosCreate()
        {
            var roles = this._usuarioService.getRoles();

            ViewBag.MyList = roles;

            return View();
        }


        [HttpPost]
        public ActionResult ABMUsuariosCreate(FormCollection formCollection)
        {
            try
            {
                if (formCollection["Name"] == "" || formCollection["Lastname"] == "" || formCollection["Email"] == "" ||
                    formCollection["Password"] == "")
                    throw new Exception("Faltan campos obligatorios");

                var usuarioDTO = new UsuariosDTO()
                {
                    Name = formCollection["Name"],
                    Lastname = formCollection["Lastname"],
                    Email = formCollection["Email"],
                    Password = Crypto.Hash(formCollection["Password"]),
                    RolId = Convert.ToInt32(formCollection["RollId"])
                };

                var hola = Convert.ToInt32(formCollection["RollId"]);

                this._usuarioService.guardarUsuario(usuarioDTO);

                return RedirectToAction("ABMUsuario", "Acceso");
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public ActionResult ABMUsuarioDelete(int id)
        {
            var usuario = this._usuarioService.GetUsuarioById(id);

            return View(usuario);
        }

        [HttpPost]
        public ActionResult ABMUsuarioDelete(int id, string param)
        {
            try
            {
                this._usuarioService.BorrarUsuario(id);

                return RedirectToAction("ABMUsuario", "Acceso");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult CambiarClaveUsuario(int id, FormCollection formCollection)
        {
            try
            {
                var pw = Crypto.Hash(formCollection["Password"]);
                this._usuarioService.CambiarClave(id, pw);

                Session.Clear();

                return RedirectToAction("Login", "Acceso");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult CambiarClaveUsuario()
        {
            try
            {
                return View();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Login", "Acceso");
        }

    }
}