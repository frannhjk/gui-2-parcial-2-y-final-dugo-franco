﻿using Parcial2yFinalDugo.Controllers;
using Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;

namespace Parcial2yFinalDugo.Filters
{
    public class VerificarSesion : ActionFilterAttribute
    {
        private UsuariosDTO usuario;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                base.OnActionExecuting(filterContext);

                usuario = (UsuariosDTO)HttpContext.Current.Session["User"];
                
                if(usuario is null)
                {
                    if (filterContext.Controller is AccesoController == false)
                        filterContext.HttpContext.Response.Redirect("/Acceso/Login");
                }
            }
            catch (Exception)
            {
                filterContext.Result = new RedirectResult("~/Acceso/Login");
            }
        }

    }
}