﻿using Service.DTOs;
using Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Parcial2yFinalDugo.ViewModels
{
    public class IndexViewModel : BaseModelo
    {
        public List<EmpresaDTO> Empresas { get; set; }
    }
}