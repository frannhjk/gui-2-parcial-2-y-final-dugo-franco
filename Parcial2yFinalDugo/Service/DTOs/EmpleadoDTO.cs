﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTOs
{
    public class EmpleadoDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string SubArea { get; set; }
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public int EmpresaId { get; set; }
        [Range(typeof(bool), "true", "true", ErrorMessage = "You gotta tick the box!")]
        public bool esGerente { get; set; } 
        public string EmpresaName { get; set; }

    }
}
