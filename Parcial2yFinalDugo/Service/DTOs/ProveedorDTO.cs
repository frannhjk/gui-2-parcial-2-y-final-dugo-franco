﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTOs
{
    public class ProveedorDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Localidad { get; set; }
        public string ImagenSrc { get; set; }
        public int NumeroEmpresasAsociadas { get; set; }
        public int EmpresaId { get; set; }

        public IEnumerable<EmpresaDTO> Empresas { get; set; }
        /* Esto lo habilito si hago un viewModel con un boton en la card
        para mostrar una view de las empresas que tieen asociadas el proveedor */
    }
}
