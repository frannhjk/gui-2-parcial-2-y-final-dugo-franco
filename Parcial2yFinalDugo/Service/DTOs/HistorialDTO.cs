﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTOs
{
    public class HistorialDTO
    {
        public int Id { get; set; }
        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }
        public bool Estado { get; set; }
        public int EmpresaId { get; set; }
        public string EmpresaName { get; set; }
        public int ProveedorId { get; set; }
        public string ProveedorName { get; set; }

        public HistorialDTO() { }
    }
}
