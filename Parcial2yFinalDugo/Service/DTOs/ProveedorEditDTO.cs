﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTOs
{
    public class ProveedorEditDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Localidad { get; set; }
        public string ImagenSrc { get; set; }
    }
}
