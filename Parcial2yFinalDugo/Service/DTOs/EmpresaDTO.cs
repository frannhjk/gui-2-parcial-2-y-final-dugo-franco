﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.DTOs
{
    public class EmpresaDTO
    {
        public int Id { get; set; }
        public string NombreComercial { get; set; }
        public string NumeroFiscal { get; set; }
        public string Area { get; set; }        
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string ImagenSrc { get; set; }

        [DataType(DataType.Date)]
        public DateTime FechaAlta { get; set; }
    }
}
