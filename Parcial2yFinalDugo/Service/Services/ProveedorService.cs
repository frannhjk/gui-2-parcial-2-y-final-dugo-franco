﻿using AutoMapper.Configuration.Conventions;
using Repository;
using Repository.Data;
using Repository.Models;
using Service.DTOs;
using Service.Helpers;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class ProveedorService : IProveedorService
    {
        private ProveedorRepository repo = new ProveedorRepository();
        private EmpresaRepository empresaRepo = new EmpresaRepository();
        private HistorialRepository historialRepo = new HistorialRepository();

        public IEnumerable<ProveedorDTO> getProveedores()
        {
            var proveedores = this.repo.getProveedores();

            var destination = Mapping.Mapper.Map<IEnumerable<ProveedorDTO>>(proveedores);

            return destination;
        }

        public ProveedorDTO getProveedor(int id)
        {
            var proveedor = this.repo.getProveedorById(id);

            var destination = Mapping.Mapper.Map<ProveedorDTO>(proveedor);

            return destination;
        }

        public ProveedorEditDTO getProveedorEdit(int id)
        {
            var prov = this.repo.getProveedorById(id);

            var destination = Mapping.Mapper.Map<Proveedor, ProveedorEditDTO>(prov);

            return destination;
        }

        public IEnumerable<int> getEmpresasIds(int id)
        {
            var resultEmpresas = this.empresaRepo.getEmpresas()
                .Select(x => x.Id).ToList();

            List<int> resultHistorial = getHistorialEmpresasIdsByProveedorId(id);

            var result = resultEmpresas.Except(resultHistorial);

            return result;
        }

        public Dictionary<int, string> getEmpresasIdsAndNames(IEnumerable<int> listIds)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            if (listIds.Count() > 0)
            {
                listIds.ToList().ForEach(a =>
                {
                    result.Add(a, this.empresaRepo.getEmpresaById(a).NombreComercial);
                });

                return result;
            }
            return result;
        }

        public List<int> getHistorialEmpresasIdsByProveedorId(int id)
        {
            return this.historialRepo.obtenerHistorialByProveedor(id)
                .Where(x => x.Estado == true)
                .Select(c => c.EmpresaId)
                .ToList();
        }

        public void Asociar(int proveedorId, int empresaId)
        {
            Historial historial = new Historial();

            var proveedor = this.repo.getProveedorById(proveedorId);
            var empresa = this.empresaRepo.getEmpresaById(empresaId);

            if (!validarAsociacion(proveedor.Id, empresa.Id))
                throw new Exception("El Proveedor ya está asociado de forma activa con la empresa seleccionada");

            if (proveedor != null && empresa != null)
            {
                proveedor.NumeroEmpresasAsociadas += 1;

                historial.EmpresaId = empresa.Id;
                historial.ProveedorId = proveedor.Id;
                historial.Fecha = DateTime.Now;
                historial.Estado = true;

                this.historialRepo.guardarHistorial(historial);
                this.repo.actualizarProveedor(proveedorId, proveedor);
            }
            else
            {
                throw new Exception("No se encontraron proveedores y/o empresas");
            }

        }

        public void DesAsociar(int proveedorId, int empresaId)
        {
            var proveedor = this.repo.getProveedorById(proveedorId);
            var empresa = this.empresaRepo.getEmpresaById(empresaId);

            if (proveedor != null && empresa != null)
            {
                this.repo.desasociarEntidades(proveedorId, empresaId);
            }
        }

        public void bajaProveedor(int id)
        {
            this.repo.borrarProveedor(id);
        }

        public void editarProveedor(int id, ProveedorEditDTO provDTO)
        {
            var destination = Mapping.Mapper.Map<ProveedorEditDTO, Proveedor>(provDTO);

            this.repo.editarProveedor(id, destination);
        }


        /* Private Methods */
        private bool validarAsociacion(int proveedirId, int empresaId)
        {
            int flag = 0;
            var historialesPorProveedor = this.historialRepo.obtenerHistorialByProveedor(proveedirId).ToList();

            if (historialesPorProveedor != null && historialesPorProveedor.Count > 0)
            {
                historialesPorProveedor.ForEach(a => {
                    if (a.EmpresaId == empresaId)
                        flag++;
                });
            }

            if (flag != 0)
                return false;
            else
                return true;
        }
    }
}
