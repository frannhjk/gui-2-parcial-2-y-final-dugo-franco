﻿using Repository.Data;
using Repository.Models;
using Service.DTOs;
using Service.Helpers;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class HistorialService : IHistorialService
    {
        private HistorialRepository historialRepo = new HistorialRepository();
        private EmpresaRepository empresaRepo = new EmpresaRepository();
        private ProveedorRepository proveedorRepo = new ProveedorRepository();

        private List<HistorialDTO> dto = new List<HistorialDTO>();

        public IEnumerable<HistorialDTO> getHistoriales()
        {
            var entities = this.historialRepo.ObtenerHistorales().ToList();

            entities.ForEach(a => {
                HistorialDTO dtoH = new HistorialDTO();
                dtoH.EmpresaId = a.EmpresaId;
                dtoH.ProveedorId = a.ProveedorId;
                dtoH.Fecha = a.Fecha;
                dtoH.Estado = a.Estado;
                dtoH.EmpresaName = this.empresaRepo.getEmpresaById(a.EmpresaId).NombreComercial;
                dtoH.ProveedorName = this.proveedorRepo.getProveedorById(a.ProveedorId).Nombre;

                dto.Add(dtoH);

            });

            return dto.OrderBy(c => c.Fecha);
        }
    }
}
