﻿using AutoMapper;
using Repository.Data;
using Repository.Models;
using Service.DTOs;
using Service.Helpers;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class UsuarioService : IUsuarioService
    {

        UsuarioRepository repo = new UsuarioRepository();


        public UsuariosDTO GetUsuario(string email, string password)
        {
            UsuariosDTO usuarioDTO = new UsuariosDTO();

            var usuario = repo.getUsuario(email, password);

            if (usuario is null)
                return usuarioDTO;

            usuarioDTO = new UsuariosDTO
            {
                Id = usuario.Id,
                Name = usuario.Name,
                Lastname = usuario.Lastname,
                Email = usuario.Email,
                Password = usuario.Password,
                RolId = usuario.RolId
            };

            return usuarioDTO;
        }

        public UsuariosDTO GetUsuarioById(int id)
        {
            var usuario = this.repo.getUsuarioById(id);

            return Mapping.Mapper.Map<UsuariosDTO>(usuario);
        }

        public bool esAdmin(Usuario usuario)
        {
            if (usuario.RolId == 1)
                return true;
            return false;
        }

        public IEnumerable<UsuariosDTO> getUsuarios()
        {

            var usuarios = this.repo.getUsuarios();
            var destination = Mapping.Mapper.Map<IEnumerable<UsuariosDTO>>(usuarios);

            return destination;
        }

        public void guardarUsuario(UsuariosDTO usuarioDTO)
        {
            var usuario = new Usuario()
            {
                Name = usuarioDTO.Name,
                Lastname = usuarioDTO.Lastname,
                Email = usuarioDTO.Email,
                Password = usuarioDTO.Password,
                RolId = 2
            };

            this.repo.guardarUsuario(usuario);
        }

        public void ActualizarUsuario(int id, UsuariosDTO usuarioDTO)
        {
            usuarioDTO.Id = id;

            var usuario = Mapping.Mapper.Map<Usuario>(usuarioDTO);

            this.repo.editarUsuario(usuario);
        }

        public void CambiarClave(int id, string newPassword)
        {
            this.repo.cambiarClave(id, newPassword);
        }

        public void BorrarUsuario(int id)
        {
            this.repo.borrarUsuario(id);
        }

        public IEnumerable<int> getRoles()
        {
            return this.repo.getRoles();
        }
    }
}
