﻿using Repository.Data;
using Repository.Models;
using Service.DTOs;
using Service.Helpers;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class EmpresaService : IEmpresaService
    {
        EmpresaRepository repo = new EmpresaRepository();

        public IEnumerable<EmpresaDTO> getEmpresas()
        {
            var empresasEntity = this.repo.getEmpresas();
            var destination = Mapping.Mapper.Map<IEnumerable<EmpresaDTO>>(empresasEntity);

            return destination;
        }

        public IEnumerable<EmpresaDTO> getEmpresasFiltered(int pagina, int cantidadRegistrosPorPagina)
        {
            var empresasEntity = this.repo.getEmpresasFiltered(pagina, cantidadRegistrosPorPagina);

            var destination = Mapping.Mapper.Map<IEnumerable<EmpresaDTO>>(empresasEntity);

            return destination;
        }

        public EmpresaDTO getEmpresa(int id)
        {
            var empresasEntity = this.repo.getEmpresaById(id);
            var destination = Mapping.Mapper.Map<EmpresaDTO>(empresasEntity);

            return destination;
        }

        public void guardarEmpresa(EmpresaDTO empresa)
        {
            var entity = Mapping.Mapper.Map<Empresa>(empresa);
            this.repo.guardarEmpresa(entity);
        }

        public void bajaEmpresa(int id)
        {
            this.repo.borrarEmpresa(id);
        }   

        public void editarEmpresa(int id, EmpresaDTO empresa)
        {
            var entity = Mapping.Mapper.Map<Empresa>(empresa);
            this.repo.editarEmpresa(id, entity);
        }

        public IEnumerable<EmpresaDTO> getEmpresasFiltered(string search)
        {
            var empresasEntities = this.repo.getEmpresasBySearch(search);

            var destination = Mapping.Mapper.Map<IEnumerable<EmpresaDTO>>(empresasEntities);

            return destination;
        }
    }
}
