﻿using Repository.Data;
using Repository.Data.Generic;
using Repository.Models;
using Service.DTOs;
using Service.Helpers;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class EmpleadoService : IEmpleadoService
    {
        private EmpleadoRepository repo = new EmpleadoRepository();
        private EmpresaRepository empresaRepo = new EmpresaRepository();

        private GenericRepository<Empleado> Repository = new GenericRepository<Empleado>();

        public void bajaEmpleado(int id)
        {
            this.repo.borrarEmpleado(id);
            // Repository.Delete(id);
        }

        public void editarEmpleado(int id, EmpleadoDTO empleado)
        {
            var entity = Mapping.Mapper.Map<Empleado>(empleado);

            // Repository.Update(entity);
            this.repo.editarEmpleado(id, entity);
        }

        public EmpleadoDTO getEmpleado(int id)
        {
            // Repository.GetById(id); 
            var empleadoEntity = this.repo.getEmpleadoById(id);

            var destination = Mapping.Mapper.Map<EmpleadoDTO>(empleadoEntity);

            var empresaName = empresaRepo.getEmpresaById(destination.EmpresaId).NombreComercial;

            destination.EmpresaName = empresaName;

            // destination.esGerente = empleadoEntity.esGerente;

            return destination;
        }

        public IEnumerable<EmpleadoDTO> getEmpleados()
        {
            // Repository.GetAll();
            var empleadosEntities = this.repo.getEmpleados();

            var destination = Mapping.Mapper.Map<IEnumerable<EmpleadoDTO>>(empleadosEntities);

            foreach(EmpleadoDTO emp in destination)
            {
                emp.EmpresaName = empresaRepo.getEmpresaById(emp.EmpresaId).NombreComercial;
            }

            return destination;
        }

        public IEnumerable<EmpleadoDTO> getEmpleadosBySearch(string search)
        {
            var empleadosEntities = this.repo.getEmpleadosBySearch(search);

            var destination = Mapping.Mapper.Map<IEnumerable<EmpleadoDTO>>(empleadosEntities);

            foreach (EmpleadoDTO emp in destination)
            {
                emp.EmpresaName = empresaRepo.getEmpresaById(emp.EmpresaId).NombreComercial;
            }

            return destination;
        }

        public void guardarEmpleado(EmpleadoDTO empleado)
        {
            var entity = Mapping.Mapper.Map<Empleado>(empleado);

            //Repository.Insert(entity);
            this.repo.guardarEmpleado(entity);
        }

        public IEnumerable<int> getEmpresasIds()
        {
            var entities = this.empresaRepo.getEmpresas()
                .Select(x => x.Id);

            return entities;
        }

        public Dictionary<int, string> getEmpresasIdsAndNames(IEnumerable<int> listIds)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();

            if (listIds.Count() > 0)
            {
                listIds.ToList().ForEach(a =>
                {
                    result.Add(a, this.empresaRepo.getEmpresaById(a).NombreComercial);
                });

                return result;
            }
            return result;
        }
    }
}
