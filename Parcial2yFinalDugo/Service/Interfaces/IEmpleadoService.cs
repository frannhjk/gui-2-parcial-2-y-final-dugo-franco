﻿using Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface IEmpleadoService
    {
        IEnumerable<EmpleadoDTO> getEmpleados();
        IEnumerable<EmpleadoDTO> getEmpleadosBySearch(string search);
        EmpleadoDTO getEmpleado(int id);
        void guardarEmpleado(EmpleadoDTO empleado);
        void bajaEmpleado(int id);
        void editarEmpleado(int id, EmpleadoDTO empleado);
        IEnumerable<int> getEmpresasIds();
        Dictionary<int, string> getEmpresasIdsAndNames(IEnumerable<int> listIds);
    }
}
