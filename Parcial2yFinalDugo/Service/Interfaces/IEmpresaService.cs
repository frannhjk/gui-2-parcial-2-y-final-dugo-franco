﻿using Repository.Models;
using Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface IEmpresaService
    {
        IEnumerable<EmpresaDTO> getEmpresas();
        IEnumerable<EmpresaDTO> getEmpresasFiltered(int pagina, int cantidadRegistrosPorPagina);
        EmpresaDTO getEmpresa(int id);
        void guardarEmpresa(EmpresaDTO empresa);
        void bajaEmpresa(int id);
        void editarEmpresa(int id, EmpresaDTO empresa);
        IEnumerable<EmpresaDTO> getEmpresasFiltered(string search);
    }
}
