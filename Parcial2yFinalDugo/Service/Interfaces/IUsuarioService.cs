﻿using Repository.Models;
using Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface IUsuarioService
    {
        IEnumerable<UsuariosDTO> getUsuarios();
        UsuariosDTO GetUsuario(string email, string password);
        UsuariosDTO GetUsuarioById(int id);
        void guardarUsuario(UsuariosDTO usuarioDTO);
        bool esAdmin(Usuario usuario);
        void ActualizarUsuario(int id, UsuariosDTO usuarioDTO);
        void CambiarClave(int id, string newPassword);
        void BorrarUsuario(int id);
        IEnumerable<int> getRoles();
    }
}
