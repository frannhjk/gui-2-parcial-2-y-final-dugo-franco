﻿using Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public interface IProveedorService
    {
        IEnumerable<ProveedorDTO> getProveedores();
        ProveedorDTO getProveedor(int id);
        ProveedorEditDTO getProveedorEdit(int id);
        IEnumerable<int> getEmpresasIds(int id);
        Dictionary<int, string> getEmpresasIdsAndNames(IEnumerable<int> listIds);
        void Asociar(int proveedorId, int empresaId);
        void DesAsociar(int proveedorId, int empresaId);
        List<int> getHistorialEmpresasIdsByProveedorId(int id);
        void bajaProveedor(int id);
        void editarProveedor(int id, ProveedorEditDTO provDTO);
    }
}
