﻿using AutoMapper;
using Repository.Models;
using Service.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Helpers
{
    public static class Mapping
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg => {
                // This line ensures that internal properties are also mapped over.
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Usuario, UsuariosDTO>();
            CreateMap<UsuariosDTO, Usuario>();

            CreateMap<Empresa, EmpresaDTO>();
            CreateMap<EmpresaDTO, Empresa>();

            CreateMap<Empleado, EmpleadoDTO>()
                .ForMember(dest => dest.EmpresaName, opts => opts.MapFrom(source => source.Empresa.NombreComercial));

            CreateMap<EmpleadoDTO, Empleado>();

            CreateMap<Proveedor, ProveedorDTO>();
            CreateMap<ProveedorDTO, Proveedor>();

            CreateMap<Proveedor, ProveedorEditDTO>();
            CreateMap<ProveedorEditDTO, Proveedor>();

            CreateMap<Historial, HistorialDTO>();
            CreateMap<HistorialDTO, Historial>();



        }
    }
}
