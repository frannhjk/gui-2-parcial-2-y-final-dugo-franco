﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class Historial
    {
        [Key]
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public bool Estado { get; set; }
        [ForeignKey("EmpresaId")]
        public Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
        [ForeignKey("ProveedorId")]
        public Proveedor Proveedor { get; set; }
        public int ProveedorId { get; set; }

        public Historial() { }
    }
}
