﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class Empresa
    {
        [Key]
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre Comercial es requerido")]
        public string NombreComercial { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "El Número Fiscal es requerido")]
        public string NumeroFiscal { get; set; }
        public string Area { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "La Dirección es requerido")]
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string ImagenSrc { get; set; }
        public DateTime FechaAlta { get; set; }
    }
}
