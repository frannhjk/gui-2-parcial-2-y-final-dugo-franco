﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class Usuario
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre es requerido")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Apellido es requerido")]
        public string Lastname { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Email es requerido")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [ForeignKey("RolId")]
        public virtual Rol Rol { get; set; }
        public int RolId { get; set; }

    }
}
