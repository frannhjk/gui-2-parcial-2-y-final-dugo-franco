﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class Empleado
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string SubArea { get; set; }
    
        [ForeignKey("EmpresaId")]
        public Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }

        public bool esGerente { get; set; } // checkbox
    }
}