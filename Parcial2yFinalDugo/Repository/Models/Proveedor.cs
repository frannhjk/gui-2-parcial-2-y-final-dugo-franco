﻿using Repository.Migrations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class Proveedor
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Localidad { get; set; }
        public string ImagenSrc { get; set; }
        public int NumeroEmpresasAsociadas { get; set; }
    }
}
