﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Data.Generic
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {

        public GenericRepository() { }

        public IEnumerable<T> GetAll()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Set<T>().ToList();
            }
        }

        public T GetById(object id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Set<T>().Find(id);
            }
        }

        public void Insert(T obj)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Set<T>().Add(obj);
                this.Save();
            }
        }

        public void Update(T obj)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Set<T>().Attach(obj);
                db.Entry(obj).State = EntityState.Modified;
                this.Save();
            }
        }

        public void Delete(object id)
        {
            using (var db = new ApplicationDbContext())
            {
                T exist = db.Set<T>().Find(id);
                db.Set<T>().Remove(exist);
                this.Save();
            }
        }

        public void Save()
        {
            using (var db = new ApplicationDbContext())
            {
                db.SaveChanges();
            }
        }
    }
}
