﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Data
{
    public class HistorialRepository
    {
        public HistorialRepository() { }

        public void guardarHistorial(Historial historial)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Historial.Add(historial);
                db.SaveChanges();
            }
        }

        public IEnumerable<Historial> ObtenerHistorales()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Historial.ToList();
            }
        }

        public IEnumerable<Historial> obtenerHistorialByProveedor(int proveedorId)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.Historial.Where(h => h.ProveedorId.Equals(proveedorId));

                if (result != null)
                    return result.ToList();

                return null;
            }
        }
    }
}
