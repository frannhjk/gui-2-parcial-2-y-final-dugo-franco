﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Data
{
    public class EmpleadoRepository
    {
        public EmpleadoRepository()
        {

        }

        public Empleado getEmpleadoById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Empleados.FirstOrDefault(u => u.Id == id);
            }
        }

        public IEnumerable<Empleado> getEmpleados()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Empleados.ToList();
            }
        }

        public IEnumerable<Empleado> getEmpleadosBySearch(string stringToSearch)
        {
            using (var db = new ApplicationDbContext())
            {
                var return1 = db.Empleados.Where(e => e.Nombre.Contains(stringToSearch));
                if (return1 == null || return1.Count() <= 0)
                    return db.Empleados.ToList();
                else
                    return return1.ToList();
            }
        }

        public void guardarEmpleado(Empleado empleado)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Empleados.Add(empleado);
                db.SaveChanges();
            }
        }

        public void borrarEmpleado(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = getEmpleadoById(id);
                db.Empleados.Attach(entity);
                db.Empleados.Remove(entity);
                db.SaveChanges();
            }
        }

        public void editarEmpleado(int id, Empleado empleado)
        {
            using (var db = new ApplicationDbContext())
            {
                var empleado1 = db.Empleados.FirstOrDefault(u => u.Id == id);

                empleado1.Nombre = empleado.Nombre;
                empleado1.Apellido = empleado.Apellido;
                empleado1.SubArea = empleado.SubArea;
                empleado1.EmpresaId = empleado.EmpresaId;
                empleado1.esGerente = empleado.esGerente;

                db.SaveChanges();
            }
        }
    }
}
