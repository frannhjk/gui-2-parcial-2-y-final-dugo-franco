﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Data
{
    public class ProveedorRepository
    {

        public ProveedorRepository() { }

        public Proveedor getProveedorById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Proveedores.FirstOrDefault(u => u.Id == id);
            }
        }

        public IEnumerable<Proveedor> getProveedores()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Proveedores.ToList();
            }
        }

        public void actualizarProveedor(int id, Proveedor proveedor)
        {
            using (var db = new ApplicationDbContext())
            {
                var pro = db.Proveedores.FirstOrDefault(x => x.Id == id);

                pro.NumeroEmpresasAsociadas = proveedor.NumeroEmpresasAsociadas;

                db.SaveChanges();
            }
        }

        public void borrarProveedor(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = getProveedorById(id);

                var historialEntity = db.Historial
                    .Where(c => c.ProveedorId == entity.Id)
                    .ToList();

                historialEntity.ForEach(est => {
                    est.Estado = false;
                    est.Fecha = DateTime.Now;
                });

                db.Proveedores.Attach(entity);
                db.Proveedores.Remove(entity);

                db.SaveChanges();
            }
        }

        public void editarProveedor(int id, Proveedor prov)
        {
            using (var db = new ApplicationDbContext())
            {
                var proveedor = db.Proveedores.FirstOrDefault(u => u.Id == id);

                if (prov.Nombre != null || prov.Nombre != "")
                    proveedor.Nombre = prov.Nombre;
                if (prov.Localidad != null || prov.Localidad != "")
                    proveedor.Localidad = prov.Localidad;
                if (prov.ImagenSrc != null || prov.ImagenSrc != "")
                    proveedor.ImagenSrc = prov.ImagenSrc;

                db.SaveChanges();
            }
        }

        public void desasociarEntidades(int proveedorId, int empresaId)
        {
            using (var db = new ApplicationDbContext())
            {
                var proveedor = db.Proveedores.FirstOrDefault(u => u.Id == proveedorId);

                var historial = db.Historial.FirstOrDefault(x => x.ProveedorId == proveedor.Id);

                proveedor.NumeroEmpresasAsociadas = proveedor.NumeroEmpresasAsociadas - 1;

                historial.Estado = false;
                historial.Fecha = DateTime.Now;

                db.SaveChanges();
            }
        }

        //public IEnumerable<Empresa> getEmpresasBySearch(string stringToSearch)
        //{
        //    using (var db = new ApplicationDbContext())
        //    {
        //        var return1 = db.Empresas.Where(e => e.NombreComercial.Contains(stringToSearch));
        //        if (return1 == null || return1.Count() <= 0)
        //            return db.Empresas.ToList();
        //        else
        //            return return1.ToList();
        //    }
        //}
    }
}
