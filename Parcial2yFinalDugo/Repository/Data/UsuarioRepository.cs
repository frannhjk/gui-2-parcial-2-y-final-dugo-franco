﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Data
{
    public class UsuarioRepository
    {
        public UsuarioRepository()
        {
            
        }

        public IEnumerable<int> getRoles()
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.Roles.ToList().Select(x => x.Id);
                return result;
            }
        }

        public Usuario getUsuarioById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Usuarios.FirstOrDefault(u => u.Id == id);
            }
        }

        public IEnumerable<Usuario> getUsuarios()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Usuarios.ToList();
            }
        }

        public Usuario getUsuario(string email, string password)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Usuarios.FirstOrDefault(u => u.Email.Equals(email) && u.Password.Equals(password));
            }
        }

        public void guardarUsuario(Usuario usuario)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
            }
        }

        public void editarUsuario(Usuario usuario)
        {

            var id = usuario.Id;

            using (var db = new ApplicationDbContext())
            {
                var usuario1 = db.Usuarios.FirstOrDefault(u => u.Id == id);

                usuario1.Name = usuario.Name;
                usuario1.Lastname = usuario.Lastname;
                usuario1.Email = usuario.Email;
                usuario1.Password = usuario.Password;
                usuario1.RolId = usuario.RolId;

                db.SaveChanges();
            }
        }

        public void borrarUsuario(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = getUsuarioById(id);
                db.Usuarios.Attach(entity);
                db.Usuarios.Remove(entity);
                db.SaveChanges();
            }
        }

        public void cambiarClave(int id, string newPassword)
        {
            using (var db = new ApplicationDbContext())
            {
                var usuario = db.Usuarios.FirstOrDefault(u => u.Id == id);
                usuario.Password = newPassword;
                db.SaveChanges();
            }
        }
    }
}
