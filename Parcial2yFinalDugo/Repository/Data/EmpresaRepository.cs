﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Data
{
    public class EmpresaRepository
    {
        public EmpresaRepository()
        {

        }

        public Empresa getEmpresaById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Empresas.FirstOrDefault(u => u.Id == id);
            }
        }

        public IEnumerable<Empresa> getEmpresas()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Empresas.ToList();
            }
        }

        public IEnumerable<Empresa> getEmpresasFiltered(int pagina, int cantidadRegistrosPorPagina)
        {
            using (var db = new ApplicationDbContext())
            {
                var empresas = db.Empresas.OrderBy(x => x.Id)
                    .Skip((pagina - 1) * cantidadRegistrosPorPagina)
                    .Take(cantidadRegistrosPorPagina).ToList();

                return empresas;
            }
        }

        public void guardarEmpresa(Empresa empresa)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Empresas.Add(empresa);
                db.SaveChanges();
            }
        }

        public void borrarEmpresa(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var entity = getEmpresaById(id);
                db.Empresas.Attach(entity);
                db.Empresas.Remove(entity);
                db.SaveChanges();
            }
        }
        
        public void editarEmpresa(int id, Empresa empresa)
        {
            using (var db = new ApplicationDbContext())
            {
                var empresa1 = db.Empresas.FirstOrDefault(u => u.Id == id);

                empresa1.NombreComercial = empresa.NombreComercial;
                empresa1.NumeroFiscal = empresa.NumeroFiscal;
                empresa1.Area = empresa.Area;
                empresa1.Direccion = empresa.Direccion;
                empresa1.Telefono = empresa.Telefono;
                empresa1.ImagenSrc = empresa.ImagenSrc;
                empresa1.FechaAlta = empresa.FechaAlta;

                db.SaveChanges();
            }
        }

        public IEnumerable<Empresa> getEmpresasBySearch(string stringToSearch)
        {
            using (var db = new ApplicationDbContext())
            {
                var return1 = db.Empresas.Where(e => e.NombreComercial.Contains(stringToSearch));
                if (return1 == null || return1.Count() <= 0)
                    return db.Empresas.ToList();
                else
                    return return1.ToList();
            }
        }
    }
}
