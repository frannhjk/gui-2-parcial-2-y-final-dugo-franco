﻿namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Empresas_V1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empresas", "ImagenSrc", c => c.String());
            AddColumn("dbo.Empresas", "FechaAlta", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Empresas", "FechaAlta");
            DropColumn("dbo.Empresas", "ImagenSrc");
        }
    }
}
