﻿namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmpresaEmpleadoV1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empleadoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Apellido = c.String(),
                        SubArea = c.String(),
                        EmpresaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empresas", t => t.EmpresaId, cascadeDelete: true)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NombreComercial = c.String(nullable: false),
                        NumeroFiscal = c.String(nullable: false),
                        Area = c.String(),
                        Direccion = c.String(nullable: false),
                        Telefono = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empleadoes", "EmpresaId", "dbo.Empresas");
            DropIndex("dbo.Empleadoes", new[] { "EmpresaId" });
            DropTable("dbo.Empresas");
            DropTable("dbo.Empleadoes");
        }
    }
}
