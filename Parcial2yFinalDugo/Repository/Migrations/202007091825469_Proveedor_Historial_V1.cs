﻿namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Proveedor_Historial_V1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Historials",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Estado = c.Boolean(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ProveedorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Empresas", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Proveedors", t => t.ProveedorId, cascadeDelete: true)
                .Index(t => t.EmpresaId)
                .Index(t => t.ProveedorId);
            
            CreateTable(
                "dbo.Proveedors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        Localidad = c.String(),
                        ImagenSrc = c.String(),
                        NumeroEmpresasAsociadas = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            DropTable("dbo.Productoes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.Historials", "ProveedorId", "dbo.Proveedors");
            DropForeignKey("dbo.Historials", "EmpresaId", "dbo.Empresas");
            DropIndex("dbo.Historials", new[] { "ProveedorId" });
            DropIndex("dbo.Historials", new[] { "EmpresaId" });
            DropTable("dbo.Proveedors");
            DropTable("dbo.Historials");
        }
    }
}
