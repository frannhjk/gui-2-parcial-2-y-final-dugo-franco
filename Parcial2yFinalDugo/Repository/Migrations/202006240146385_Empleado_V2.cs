﻿namespace Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Empleado_V2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empleadoes", "esGerente", c => c.Boolean(nullable: false));
            DropColumn("dbo.Empleadoes", "esGerenete");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Empleadoes", "esGerenete", c => c.Boolean(nullable: false));
            DropColumn("dbo.Empleadoes", "esGerente");
        }
    }
}
