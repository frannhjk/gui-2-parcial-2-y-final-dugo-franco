
DECLARE @cont int 
SET @cont = (SELECT COUNT(*) FROM Rols)


IF (@cont < 1)
BEGIN
	-- Crear Roles
	INSERT INTO Rols (Name)
	VALUES('Administrador')

	INSERT INTO Rols (Name)
	VALUES('Normal')
END

-- Set admin user 
UPDATE Usuarios SET  RolId = 1 WHERE id = ''

-- Crear Proveedores
INSERT INTO Proveedors(Nombre, Localidad, ImagenSrc, NumeroEmpresasAsociadas)
VALUES('Intel', 'Palo Alto', 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.qUgPS4hXe9FPHKg52s58TQHaE8%26pid%3DApi&f=1', 0)

INSERT INTO Proveedors(Nombre, Localidad, ImagenSrc, NumeroEmpresasAsociadas)
VALUES('AMD', 'Palo Alto', 'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F1.bp.blogspot.com%2F-T9mmhti40Wo%2FUAFdCQjPZqI%2FAAAAAAAAAL4%2F6fxKBk2BZ8E%2Fs1600%2FAmd%2B2.png&f=1&nofb=1', 0)

INSERT INTO Proveedors(Nombre, Localidad, ImagenSrc, NumeroEmpresasAsociadas)
VALUES('Microsoft', 'New York', 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.xUAVW33u4G82g7eF69W0xAHaHV%26pid%3DApi&f=1', 0)

INSERT INTO Proveedors(Nombre, Localidad, ImagenSrc, NumeroEmpresasAsociadas)
VALUES('UCES', 'Buenos Aires', 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fstatic.acaula.com.ar%2Fcenter%2FAR%2F22-uces-universidad-de-ciencias-empresariales-y-sociales-a04b3c26-17d2-4825-be20-2537656d847c-logo-200x200.png&f=1&nofb=1', 0)



-- Crear Empresas 
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Pepsico', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.LmZzZBnZ9yaQKIYFaYTRUwHaHa%26pid%3DApi&f=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Coca Cola', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmodernseoul.files.wordpress.com%2F2015%2F09%2Fcoca-cola-logo.png&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Laravel', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F9%2F9a%2FLaravel.svg%2F1200px-Laravel.svg.png&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Wallmart', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.isvnmvx5JWPtrtdDCQ_f7wHaGL%26pid%3DApi&f=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Apple', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Ff%2Ffa%2FApple_logo_black.svg%2F1200px-Apple_logo_black.svg.png&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Sony', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid%3DOIP.NWgn8O_rv4_JI9RbqxJZDwAAAA%26pid%3DApi&f=1s', getdate())


-- Para ver Paginado
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa A', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa B', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa C', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa D', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa E', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa F', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa G', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa H', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa I', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa J', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa K', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa L', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa M', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())
INSERT INTO Empresas(NombreComercial, NumeroFiscal, Area, Direccion, Telefono, ImagenSrc, FechaAlta)
VALUES('Empresa N', '123', 'SA', 'SA 123', 1231322, 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.wired.com%2Fphotos%2F5b17381815b2c744cb650b5f%2Fmaster%2Fw_2000%2Cc_limit%2FGettyImages-134367495.jpg&f=1&nofb=1', getdate())


-- SELECTS
SELECT * FROM Rols

SELECT * FROM Usuarios

SELECT * FROM Proveedors

SELECT * FROM Empresas

SELECT * FROM Empleadoes